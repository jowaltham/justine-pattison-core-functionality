<?php
/**
 * Plugin Name: Justine Pattison Core Functionality
 * Plugin URI: https://github.com/billerickson/Core-Functionality
 * Description: This contains all your site's core functionality so that it is theme independent.
 * Version: 2.0.0
 * Author: Jo Waltham
 * Author URI: https://www.calliaweb.co.uk
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License version 2, as published by the Free Software Foundation.  You may NOT assume
 * that you can use any other version of the GPL.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

// Plugin Directory
define( 'JMW_DIR', dirname( __FILE__ ) );

require_once( JMW_DIR . '/inc/general.php' );
require_once( JMW_DIR . '/inc/custom-post-types.php' );
require_once( JMW_DIR . '/inc/custom-taxonomies.php' );
//require_once( JMW_DIR . '/inc/custom-options.php' );
