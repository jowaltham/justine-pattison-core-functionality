<?php
/**
 * Core Functionality Plugin
 *
 * @package    SchoolsmithCoreFunctionality
 * @since      2.0.0
 * @copyright  Copyright (c) 2017, Jo Waltham
 * @license    GPL-2.0+
 */


 /**
 * Dont Update the Plugin
 * If there is a plugin in the repo with the same name, this prevents WP from prompting an update.
 *
 * @since  1.0.0
 * @author Jon Brown
 * @param  array $r Existing request arguments
 * @param  string $url Request URL
 * @return array Amended request arguments
 */
function be_dont_update_core_func_plugin( $r, $url ) {
  if ( 0 !== strpos( $url, 'https://api.wordpress.org/plugins/update-check/1.1/' ) )
    return $r; // Not a plugin update request. Bail immediately.
    $plugins = json_decode( $r['body']['plugins'], true );
    unset( $plugins['plugins'][plugin_basename( __FILE__ )] );
    $r['body']['plugins'] = json_encode( $plugins );
    return $r;
 }
add_filter( 'http_request_args', 'be_dont_update_core_func_plugin', 5, 2 );

// Use shortcodes in widgets
add_filter( 'widget_text', 'do_shortcode' );

// Don't let WPSEO metabox be high priority
add_filter( 'wpseo_metabox_prio', function(){ return 'low'; } );

add_filter('tiny_mce_before_init', 'jmw_tiny_mce_remove_unused_formats' );
/*
 * Modify TinyMCE editor to remove H1.
 */
function jmw_tiny_mce_remove_unused_formats($init) {
    // Add block format elements you want to show in dropdown
    $init['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6;Address=address;Pre=pre';

    return $init;
}

add_action('admin_init', 'jmw_imagelink_setup', 10);
/**
 * Remove the image link option
 */
function jmw_imagelink_setup() {
    $image_set = get_option( 'image_default_link_type' );

    if ($image_set !== 'none') {
        update_option('image_default_link_type', 'none');
    }
}

// Change name of the WP Recipe Maker CPT
add_filter( 'wprm_recipe_post_type_arguments', 'jmw_change_wprm_recipe_post_type_arguments' );
function jmw_change_wprm_recipe_post_type_arguments( $args ) {
  $new_args = $args;
  $new_args[ 'labels' ][ 'name' ] = 'Recipe Maker';
  $new_args[ 'labels' ][ 'singular_name' ] = 'Recipe Maker';

  return $new_args;
}