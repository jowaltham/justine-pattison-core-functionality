<?php

/**
 * Add school options
 */

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title'    => 'School Report Descriptions',
        'menu_title'    => 'School Report Descriptions',
        'menu_slug'     => 'school-report-descriptions',
        'capability'    => 'edit_posts',
        'redirect'      => false,
        'position'		=> '6.6'
    ));

}