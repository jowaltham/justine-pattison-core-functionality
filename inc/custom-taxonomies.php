<?php

// Register Custom Taxonomy
add_action( 'init', 'register_recipe_category_taxonomy', 0 );
add_action( 'init', 'register_howto_category_taxonomy', 0 );

function register_recipe_category_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Recipe Categories', 'Taxonomy General Name', 'justine-pattison-core-functionality' ),
		'singular_name'              => _x( 'Recipe Category', 'Taxonomy Singular Name', 'justine-pattison-core-functionality' ),
		'menu_name'                  => __( 'Recipe Categories', 'justine-pattison-core-functionality' ),
		'all_items'                  => __( 'All Recipe Categories', 'justine-pattison-core-functionality' ),
		'parent_item'                => __( 'Parent Item', 'justine-pattison-core-functionality' ),
		'parent_item_colon'          => __( 'Parent Item:', 'justine-pattison-core-functionality' ),
		'new_item_name'              => __( 'New Recipe Category', 'justine-pattison-core-functionality' ),
		'add_new_item'               => __( 'Add New Recipe Category', 'justine-pattison-core-functionality' ),
		'edit_item'                  => __( 'Edit Recipe Category', 'justine-pattison-core-functionality' ),
		'update_item'                => __( 'Update Recipe Category', 'justine-pattison-core-functionality' ),
		'view_item'                  => __( 'View Recipe Category', 'justine-pattison-core-functionality' ),
		'separate_items_with_commas' => __( 'Separate Recipe Categories with commas', 'justine-pattison-core-functionality' ),
		'add_or_remove_items'        => __( 'Add or remove Recipe Categories', 'justine-pattison-core-functionality' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'justine-pattison-core-functionality' ),
		'popular_items'              => __( 'Popular Recipe Categories', 'justine-pattison-core-functionality' ),
		'search_items'               => __( 'Search Recipe Categories', 'justine-pattison-core-functionality' ),
		'not_found'                  => __( 'Not Found', 'justine-pattison-core-functionality' ),
		'no_terms'                   => __( 'No Recipe Categories', 'justine-pattison-core-functionality' ),
		'items_list'                 => __( 'Recipe Categories list', 'justine-pattison-core-functionality' ),
		'items_list_navigation'      => __( 'Recipe Categories list navigation', 'justine-pattison-core-functionality' ),
	);
	$rewrite = array(
		'slug'                       => 'recipe-category',
		'with_front'                 => true,
		'hierarchical'               => false,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
		'query_var'                  => 'recipe-category',
		'rewrite'                    => $rewrite,
	);
	register_taxonomy( 'jp_recipe_category', array( 'jp_recipes' ), $args );

}


// Register Custom Taxonomy
function register_howto_category_taxonomy() {

	$labels = array(
		'name'                       => _x( 'How To Categories', 'Taxonomy General Name', 'justine-pattison-core-functionality' ),
		'singular_name'              => _x( 'How To Category', 'Taxonomy Singular Name', 'justine-pattison-core-functionality' ),
		'menu_name'                  => __( 'How To Categories', 'justine-pattison-core-functionality' ),
		'all_items'                  => __( 'All How To Categories', 'justine-pattison-core-functionality' ),
		'parent_item'                => __( 'Parent Item', 'justine-pattison-core-functionality' ),
		'parent_item_colon'          => __( 'Parent Item:', 'justine-pattison-core-functionality' ),
		'new_item_name'              => __( 'New How To Category', 'justine-pattison-core-functionality' ),
		'add_new_item'               => __( 'Add New How To Category', 'justine-pattison-core-functionality' ),
		'edit_item'                  => __( 'Edit How To Category', 'justine-pattison-core-functionality' ),
		'update_item'                => __( 'Update How To Category', 'justine-pattison-core-functionality' ),
		'view_item'                  => __( 'View How To Category', 'justine-pattison-core-functionality' ),
		'separate_items_with_commas' => __( 'Separate How To Categories with commas', 'justine-pattison-core-functionality' ),
		'add_or_remove_items'        => __( 'Add or remove How To Categories', 'justine-pattison-core-functionality' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'justine-pattison-core-functionality' ),
		'popular_items'              => __( 'Popular How To Categories', 'justine-pattison-core-functionality' ),
		'search_items'               => __( 'Search How To Categories', 'justine-pattison-core-functionality' ),
		'not_found'                  => __( 'Not Found', 'justine-pattison-core-functionality' ),
		'no_terms'                   => __( 'No How To Categories', 'justine-pattison-core-functionality' ),
		'items_list'                 => __( 'How To Categories list', 'justine-pattison-core-functionality' ),
		'items_list_navigation'      => __( 'How To Categories list navigation', 'justine-pattison-core-functionality' ),
	);
	$rewrite = array(
		'slug'                       => 'how-to-category',
		'with_front'                 => true,
		'hierarchical'               => false,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
		'query_var'                  => 'how-to-category',
		'rewrite'                    => $rewrite,
	);
	register_taxonomy( 'jp_howto_category', array( 'jp_howto' ), $args );

}