<?php

// Register Custom Post Types
add_action( 'init', 'register_jp_recipes_post_type', 0 );
add_action( 'init', 'register_jp_howto_post_type', 0 );
add_action( 'init', 'add_post_type_support_to_tribe_events', 0 );

function register_jp_recipes_post_type() {

	$labels = array(
		'name'                  => _x( 'Recipes', 'Post Type General Name', 'justine-pattison-core-functionality' ),
		'singular_name'         => _x( 'Recipe', 'Post Type Singular Name', 'justine-pattison-core-functionality' ),
		'menu_name'             => __( 'Recipes', 'justine-pattison-core-functionality' ),
		'name_admin_bar'        => __( 'Recipes', 'justine-pattison-core-functionality' ),
		'archives'              => __( 'Recipes Archives', 'justine-pattison-core-functionality' ),
		'attributes'            => __( 'Recipes Attributes', 'justine-pattison-core-functionality' ),
		'parent_item_colon'     => __( 'Recipe Item:', 'justine-pattison-core-functionality' ),
		'all_items'             => __( 'All Recipes', 'justine-pattison-core-functionality' ),
		'add_new_item'          => __( 'Add New Recipe', 'justine-pattison-core-functionality' ),
		'add_new'               => __( 'Add Recipe', 'justine-pattison-core-functionality' ),
		'new_item'              => __( 'New Recipe', 'justine-pattison-core-functionality' ),
		'edit_item'             => __( 'Edit Recipe', 'justine-pattison-core-functionality' ),
		'update_item'           => __( 'Update Recipe', 'justine-pattison-core-functionality' ),
		'view_item'             => __( 'View Recipe', 'justine-pattison-core-functionality' ),
		'view_items'            => __( 'View Recipes', 'justine-pattison-core-functionality' ),
		'search_items'          => __( 'Search Recipe', 'justine-pattison-core-functionality' ),
		'not_found'             => __( 'Recipe Not found', 'justine-pattison-core-functionality' ),
		'not_found_in_trash'    => __( 'Recipe Not found in Trash', 'justine-pattison-core-functionality' ),
		'featured_image'        => __( 'Recipe Featured Image', 'justine-pattison-core-functionality' ),
		'set_featured_image'    => __( 'Set featured image', 'justine-pattison-core-functionality' ),
		'remove_featured_image' => __( 'Remove featured image', 'justine-pattison-core-functionality' ),
		'use_featured_image'    => __( 'Use as featured image', 'justine-pattison-core-functionality' ),
		'insert_into_item'      => __( 'Insert into Recipe', 'justine-pattison-core-functionality' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Recipe', 'justine-pattison-core-functionality' ),
		'items_list'            => __( 'Recipes list', 'justine-pattison-core-functionality' ),
		'items_list_navigation' => __( 'Recipes list navigation', 'justine-pattison-core-functionality' ),
		'filter_items_list'     => __( 'Filter Recipes list', 'justine-pattison-core-functionality' ),
	);
	$rewrite = array(
		'slug'                  => 'recipe',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Recipe', 'justine-pattison-core-functionality' ),
		'description'           => __( 'Recipes', 'justine-pattison-core-functionality' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'author', 'genesis-cpt-archives-settings', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-carrot',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => __( 'recipes', 'justine-pattison-core-functionality' ),
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
	);
	register_post_type( 'jp_recipes', $args );
}

function register_jp_howto_post_type() {

	$labels = array(
		'name'                  => _x( 'How To', 'Post Type General Name', 'justine-pattison-core-functionality' ),
		'singular_name'         => _x( 'How To', 'Post Type Singular Name', 'justine-pattison-core-functionality' ),
		'menu_name'             => __( 'How To', 'justine-pattison-core-functionality' ),
		'name_admin_bar'        => __( 'How To', 'justine-pattison-core-functionality' ),
		'archives'              => __( 'How To Archives', 'justine-pattison-core-functionality' ),
		'attributes'            => __( 'How To Attributes', 'justine-pattison-core-functionality' ),
		'parent_item_colon'     => __( 'How To Item:', 'justine-pattison-core-functionality' ),
		'all_items'             => __( 'All How To', 'justine-pattison-core-functionality' ),
		'add_new_item'          => __( 'Add New How To', 'justine-pattison-core-functionality' ),
		'add_new'               => __( 'Add How To', 'justine-pattison-core-functionality' ),
		'new_item'              => __( 'New How To', 'justine-pattison-core-functionality' ),
		'edit_item'             => __( 'Edit How To', 'justine-pattison-core-functionality' ),
		'update_item'           => __( 'Update How To', 'justine-pattison-core-functionality' ),
		'view_item'             => __( 'View How To', 'justine-pattison-core-functionality' ),
		'view_items'            => __( 'View How To', 'justine-pattison-core-functionality' ),
		'search_items'          => __( 'Search How To', 'justine-pattison-core-functionality' ),
		'not_found'             => __( 'How To Not found', 'justine-pattison-core-functionality' ),
		'not_found_in_trash'    => __( 'How To Not found in Trash', 'justine-pattison-core-functionality' ),
		'featured_image'        => __( 'How To Featured Image', 'justine-pattison-core-functionality' ),
		'set_featured_image'    => __( 'Set featured image', 'justine-pattison-core-functionality' ),
		'remove_featured_image' => __( 'Remove featured image', 'justine-pattison-core-functionality' ),
		'use_featured_image'    => __( 'Use as featured image', 'justine-pattison-core-functionality' ),
		'insert_into_item'      => __( 'Insert into How To', 'justine-pattison-core-functionality' ),
		'uploaded_to_this_item' => __( 'Uploaded to this How To', 'justine-pattison-core-functionality' ),
		'items_list'            => __( 'How To list', 'justine-pattison-core-functionality' ),
		'items_list_navigation' => __( 'How To list navigation', 'justine-pattison-core-functionality' ),
		'filter_items_list'     => __( 'Filter How To list', 'justine-pattison-core-functionality' ),
	);
	$rewrite = array(
		'slug'                  => 'how-to',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'How To', 'justine-pattison-core-functionality' ),
		'description'           => __( 'How To', 'justine-pattison-core-functionality' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'author', 'genesis-cpt-archives-settings', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-book',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => __( 'how-to', 'justine-pattison-core-functionality' ),
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
	);
	register_post_type( 'jp_howto', $args );
}

function add_post_type_support_to_tribe_events() {
	add_post_type_support( 'tribe_events', 'genesis-cpt-archives-settings' );
}